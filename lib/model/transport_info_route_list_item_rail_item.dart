class TransportInfoRouteListItemRailItem {
  String railLinkId;

  TransportInfoRouteListItemRailItem(this.railLinkId);

  factory TransportInfoRouteListItemRailItem.fromJson(Map<String, dynamic> json) {
    return TransportInfoRouteListItemRailItem(
      json['railLinkId'],
    );
  }
}