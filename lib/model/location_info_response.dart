import 'package:app_api_test_public_transport/model/location_info_list.dart';

class LocationInfoResponse {
  LocationInfoList msgBody;

  LocationInfoResponse(this.msgBody);

  factory LocationInfoResponse.fromJson(Map<String, dynamic> json) {
    return LocationInfoResponse(
      LocationInfoList.formJson(json['msgBody']),
    );
  }
}