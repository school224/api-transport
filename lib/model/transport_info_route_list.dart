import 'package:app_api_test_public_transport/model/transport_info_route_list_item.dart';

class TransportInfoRouteList {
  int time;
  num distance;
  List<TransportInfoRouteListItem> transportList;

  TransportInfoRouteList (this.time, this.distance, this.transportList);

  factory TransportInfoRouteList.fromJson(Map<String, dynamic> json) {
    return TransportInfoRouteList(
      int.parse(json['time']),
      (num.parse(json['distance']) / 1000),
      json['transportList'] == null ? [] : (json['transportList'] as List).map((e) => TransportInfoRouteListItem.fromJson(e)).toList(),
    );
  }
}