class LocationInfoListItem {
  int poiId; //poi ID
  String poiNm; //poi 이름
  num gpsX; //x좌표 (WGS84)
  num gpsY; //y좌표 (WGS84)

  LocationInfoListItem(
      this.poiId,
      this.poiNm,
      this.gpsX,
      this.gpsY,
      );

  factory LocationInfoListItem.fromJson(Map<String, dynamic> json) {
    return LocationInfoListItem(
      int.parse(json['poiId']),
      json['poiNm'],
      num.parse(json['gpsX']),
      num.parse(json['gpsY']),
    );
  }
}