class TransportInfoRequest {
  num startX;
  num startY;
  num endX;
  num endY;

  TransportInfoRequest(this.startX, this.startY, this.endX, this.endY);
}