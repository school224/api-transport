import 'location_info_list_item.dart';

class LocationInfoList {
  List<LocationInfoListItem> itemList;

  LocationInfoList(this.itemList);

  factory LocationInfoList.formJson(Map<String, dynamic> json) {
    return LocationInfoList(
        json['itemList'] == null ? [] : (json['itemList'] as List).map((e) => LocationInfoListItem.fromJson(e)).toList()
    );
  }
}