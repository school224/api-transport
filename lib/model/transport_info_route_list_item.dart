import 'package:app_api_test_public_transport/model/transport_info_route_list_item_rail_item.dart';

class TransportInfoRouteListItem {
  String routeNm;
  String fname;
  String tname;
  bool isBus;

  TransportInfoRouteListItem(
      this.routeNm,
      this.fname,
      this.tname,
      this.isBus,
      );

  factory TransportInfoRouteListItem.fromJson(Map<String, dynamic> json) {
    List<TransportInfoRouteListItemRailItem>? railItems = json['railLinkList'] == null ? [] : (json['railLinkList'] as List).map((e) => TransportInfoRouteListItemRailItem.fromJson(e)).toList();
    bool isBus = true;
    if (railItems.length > 0) isBus = false;

    //todo : 삼항연산자를 이용해서 3줄 -> 1줄로 줄일수있다. 한번 생각해보기.

    return TransportInfoRouteListItem(
      json['routeNm'],
      json['fname'],
      json['tname'],
      isBus,
    );
  }
}