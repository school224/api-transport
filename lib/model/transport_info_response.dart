import 'package:app_api_test_public_transport/model/transport_info_route_response.dart';

class TransportInfoResponse {
  TransportInfoRouteResponse msgBody;

  TransportInfoResponse(this.msgBody);

  factory TransportInfoResponse.fromJson(Map<String, dynamic> json) {
    return TransportInfoResponse (
      TransportInfoRouteResponse.fromJson(json['msgBody']),
    );
  }
}