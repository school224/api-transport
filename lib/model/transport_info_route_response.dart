import 'package:app_api_test_public_transport/model/transport_info_route_list.dart';

class TransportInfoRouteResponse {
  List<TransportInfoRouteList> itemList;

  TransportInfoRouteResponse(this.itemList);

  factory TransportInfoRouteResponse.fromJson(Map<String, dynamic> json) {
    return TransportInfoRouteResponse(
      json['itemList'] == null ? [] : (json['itemList'] as List).map((e) => TransportInfoRouteList.fromJson(e)).toList(),
    );
  }
}