import 'package:app_api_test_public_transport/model/transport_info_route_list.dart';
import 'package:flutter/material.dart';

class ComponentRouteListItem extends StatelessWidget {
  const ComponentRouteListItem({super.key, required this.item});

  final TransportInfoRouteList item;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.pink, width: 5),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      child: Column(
        children: [
          for (int i = 0;  i < item.transportList.length; i++)
            Text('${item.transportList[i].isBus ? '[버스]' : '[지하철]'} ${item.transportList[i].routeNm} ${item.transportList[i].fname} -> ${item.transportList[i].tname}'),
          Text('총 소요시간 : ${item.time}분 / 거리 : ${item.distance} km'),
        ],
      ),
    );
  }
}
