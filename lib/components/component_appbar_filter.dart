import 'package:flutter/material.dart';

// PreferredSizeWidget 위젯 사이즈를 조정.
// 앱바같은 경우 기본으로 높이가 고정되어있으나 PreferredSizeWidget을 구현(implements) 함으로써
// 높이를 조절 할 수 있게 됨.
class ComponentAppbarFilter extends StatefulWidget implements PreferredSizeWidget {
  const ComponentAppbarFilter({super.key, required this.title, required this.isUseActionButton, required this.actionIcon, required this.callback});

  final String title;
  final bool isUseActionButton;
  final IconData actionIcon;
  final VoidCallback callback;

  @override
  State<ComponentAppbarFilter> createState() => _ComponentAppbarFilterState();

  // 위젯의 높이를 40dp로 고정
  @override
  Size get preferredSize {
    return const Size.fromHeight(40);
  }
}

class _ComponentAppbarFilterState extends State<ComponentAppbarFilter> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false, // 타이틀(글자) 위치 가운데로 하겠니?
      automaticallyImplyLeading: false, // 타이틀(글자) 앞에 자동으로 뒤로가기버튼 같은거 달리는거 허용하겠니?
      title: Text(widget.title,
      style: TextStyle(fontFamily: 'NanumBrushScript', fontSize: 30, color: Colors.black),),
      backgroundColor: Colors.white.withOpacity(0.50),
      elevation: 1.0, // 앱바 아래 그림자처럼 보이는 것
      actions: [
        widget.isUseActionButton ? IconButton(onPressed: widget.callback, icon: Icon(widget.actionIcon)) : const Center(),
      ],
    );
  }
}
