import 'package:flutter/material.dart';

class ComponentNoContents extends StatelessWidget {
  const ComponentNoContents({super.key, required this.icon, required this.msg});

  final IconData icon;
  final String msg;


  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            size: 60,
            color: Colors.white,
          ),
          const SizedBox(
            height: 15,
          ),
          Text(
            msg,
            style: const TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.bold
            ),
          ),
        ],
      ),
    );
  }
}
