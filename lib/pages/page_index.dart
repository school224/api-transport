import 'package:app_api_test_public_transport/components/component_appbar_filter.dart';
import 'package:app_api_test_public_transport/components/component_count_title.dart';
import 'package:app_api_test_public_transport/components/component_no_contents.dart';
import 'package:app_api_test_public_transport/components/component_route_list_item.dart';
import 'package:app_api_test_public_transport/model/transport_info_request.dart';
import 'package:app_api_test_public_transport/model/transport_info_route_list.dart';
import 'package:app_api_test_public_transport/pages/page_search_location.dart';
import 'package:app_api_test_public_transport/repository/repo_transport_info.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();

  String startAreaName = '';
  num startAreaX = 0;
  num startAreaY = 0;
  String endAreaName = '';
  num endAreaX = 0;
  num endAreaY = 0;

  List<TransportInfoRouteList> _itemList = [];

  Future<void> _loadItems() async {
    if (startAreaX != 0 && startAreaY != 0 && endAreaX != 0 && endAreaY != 0) {
      TransportInfoRequest request =
      TransportInfoRequest(startAreaX, startAreaY, endAreaX, endAreaY);

      await RepoTransportInfo().getList(request).then((res) {
        setState(() {
          _itemList = res.msgBody.itemList;
        });
      }).catchError((err) => debugPrint(err));

      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    } else {
      BotToast.showSimpleNotification(
        title: "출발지와 도착지는 필수입니다.",
        subTitle: "출발지와 도착지를 먼저 전부 검색해주세요.",
        enableSlideOff: true,
        hideCloseButton: false,
        onlyOne: true,
        crossPage: true,
        backButtonBehavior: BackButtonBehavior.none,
        animationDuration: Duration(milliseconds: 200),
        animationReverseDuration: Duration(milliseconds: 200),
        duration: Duration(seconds: 2),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '서울에서 길찾기',
        isUseActionButton: false,
        actionIcon: Icons.bus_alert,
        callback: () {},
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(1, 2, 36, 1),
              border: Border.all(
                width: 10,
                color: Colors.pink,
              ),
            ),
            child: Column(
              children: [
                Image.asset('assets/g20.gif',width: 200, height: 200,),
              ],
            ),
          ),
          _buildForm(),
          const Divider(),
          _buildList(),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          '출발지',
          style: TextStyle(
              fontFamily: 'NanumBrushScript',
              fontSize: 30,
              color: Colors.black),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text(startAreaName),
            ),
            IconButton(
                onPressed: () async {
                  final popupResult = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageSearchLocation(isStart: true),
                      ),
                  );

                  if (popupResult != null && popupResult[0] && popupResult[1]) {
                    setState(() {
                      startAreaName = popupResult[2];
                      startAreaX = popupResult[3];
                      startAreaY = popupResult[4];
                    });
                  }
                },
                icon: const Icon(Icons.search),
            ),
          ],
        ),
        const SizedBox(
          height: 15,
        ),
        const Text(
          '도착지',
          style: TextStyle(
              fontFamily: 'NanumBrushScript',
              fontSize: 30,
              color: Colors.black),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text(endAreaName),
            ),
            IconButton(
                onPressed: () async {
                  final popupResult = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageSearchLocation(isStart: false),
                    ),
                  );

                  if (popupResult != null && popupResult[0] && !popupResult[1]) {
                    setState(() {
                      endAreaName = popupResult[2];
                      endAreaX = popupResult[3];
                      endAreaY = popupResult[4];
                    });
                  }
                },
                icon: const Icon(Icons.search),
            ),
          ],
        ),
        const SizedBox(height: 15,),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.pink
          ),
            onPressed: () {
              _loadItems();
            },
            child: Text('조회', style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),),
        ),
      ],
    );
  }

  Widget _buildList() {
    if (_itemList.length > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(
            icon: Icons.bus_alert,
            count: _itemList.length,
            unitName: '건',
            itemName: '경로',
          ),
          const SizedBox(height: 15,),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 5,
            itemBuilder: (_, index) => ComponentRouteListItem(item: _itemList[index]),),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 40,
        child: const ComponentNoContents(icon: Icons.factory, msg: '경로가 없습니다'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}
