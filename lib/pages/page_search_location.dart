import 'package:app_api_test_public_transport/components/component_appbar_popup.dart';
import 'package:app_api_test_public_transport/model/location_info_list_item.dart';
import 'package:app_api_test_public_transport/repository/repo_location_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageSearchLocation extends StatefulWidget {
  const PageSearchLocation({super.key, required this.isStart});

  final bool isStart;

  @override
  State<PageSearchLocation> createState() => _PageSearchLocationState();
}

class _PageSearchLocationState extends State<PageSearchLocation> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

  List<LocationInfoListItem> _list = [];

  Future<void> _loadItems(String keyword) async {
    await RepoLocationInfo().getList(keyword).then((res) {
      setState(() {
        _list = res.msgBody.itemList;
      });
    }).catchError((err) => print(err));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '${widget.isStart ? '출발지' : '도착지'} 검색'),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: FormBuilderTextField(
            style: TextStyle(
                fontFamily: 'NanumBrushScript',
                fontSize: 30,
                color: Colors.black
            ),
            name: 'searchKeyword',
            decoration: const InputDecoration(
              labelText: '역 또는 정류장 이름',
            ),
          ),
        ),
        const SizedBox(height: 10,),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.pink,
          ),
            onPressed: () {
                String searchKeyword = _formKey.currentState!.fields['searchKeyword']!.value;
                _loadItems(searchKeyword);
            },
            child: const Text('검색', style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),),
        ),
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: _list.length,
          itemBuilder: (_, index) => Column(
            children: [
              Text('${_list[index].poiNm} (${_list[index].gpsX}, ${_list[index].gpsY})'),
              IconButton(
                  onPressed: () {
                    Navigator.pop(
                        context,
                        [true, widget.isStart, _list[index].poiNm, _list[index].gpsX, _list[index].gpsY],
                    );
                  },
                  icon: const Icon(Icons.check),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
